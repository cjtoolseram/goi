package main  

import (
	"net/http"
	"html/template"
)

var homeTmpl = template.Must(template.New("home").ParseFiles("templates/base.html", "templates/navigation.html" , "templates/home.html"))

func homeHandler(w http.ResponseWriter, r *http.Request) {
	homeTmpl.ExecuteTemplate(w, "base", "home")
}

var chaptersTmpl = template.Must(template.New("chapters").ParseFiles("templates/base.html", "templates/navigation.html" , "templates/chapters.html"))

func chaptersHandler(w http.ResponseWriter, r *http.Request) {
	chaptersTmpl.ExecuteTemplate(w, "base", "chapters")
}

func main() {
	http.HandleFunc("/", homeHandler)
	http.HandleFunc("/chapters", chaptersHandler)
	http.ListenAndServe("localhost:8080", nil)
}

